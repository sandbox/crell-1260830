<?php

namespace Drupal\Context;

/**
 * Exception thrown when attempting to use an unlocked context object.
 */
class NotLockedException extends ContextException {}
