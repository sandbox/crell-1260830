<?php

namespace Drupal\Context;

/**
 * Base class for Context-related exceptions.
 */
class ContextException extends \Exception {}