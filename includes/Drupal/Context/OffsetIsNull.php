<?php

namespace Drupal\Context;


/**
 * Internal class that is meant for context values existence check optimization.
 *
 * Do not use elsewhere.
 */
class OffsetIsNull {}
