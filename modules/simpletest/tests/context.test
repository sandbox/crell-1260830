<?php

/**
 * Unit tests for the context system.
 *
 * Note: This file is using full namespaced names for classes, but in practice
 * most code would use a "use" directive and then just the short class name.
 */

class ContextTestCases extends DrupalUnitTestCase {
  public function setUp() {
    parent::setUp();

    require_once(DRUPAL_ROOT . '/modules/simpletest/tests/context_test.module');

    // This unfortunately necessary because Drupal's registry throws a database
    // exception when testing class_exist on non-existent classes in unit tests.
    // This is the point of one of our tests so we have to remove the registry
    // prior to running our tests.
    spl_autoload_unregister('drupal_autoload_class');
    spl_autoload_unregister('drupal_autoload_interface');
  }

  public function tearDown() {
    // Re-register drupal's autoload classes. See setUp for the reasoning.
    spl_autoload_register('drupal_autoload_class');
    spl_autoload_register('drupal_autoload_interface');

    parent::tearDown();
  }
}

class ContextTestCase extends ContextTestCases {
  public static function getInfo() {
    return array(
      'name' => 'Context functionality',
      'description' => 'Test the Context context object.',
      'group' => 'Context',
    );
  }

  /**
   * Test simple handler registration.
   */
  function testSimpleRegistration() {
    $butler = new \Drupal\Context\Context();

    $butler->setHandler('http:get', 'ContextMockHandler', array('values' => array('foo' => 'bar')));
    $butler->lock();

    $param = $butler->getValue('http:get:foo');

    $this->assertEqual($param, 'bar', t('Correct Http GET fragment found.'));
  }

  /**
   * Test explicit specification of a context value.
   */
  function testExplicitContext() {
    $butler = new \Drupal\Context\Context();

    $butler->setValue('foo:bar', 'baz');

    $butler->lock();

    $this->assertEqual($butler->getValue('foo:bar'), 'baz', t('Explicit context set correctly.'));
  }

  /**
   * Test explicit overriding of a context value.
   */
  function testExplicitContextOverride() {
    $butler = new \Drupal\Context\Context();

    // This handler would return "one".
    $butler->setHandler('foo:bar', 'ContextTestCaseHelperOne');

    // But we override it to "two".
    $butler->setValue('foo:bar', 'two');

    $butler->lock();

    $this->assertEqual($butler->getValue('foo:bar'), 'two', t('Explicit context overridden correctly.'));
  }

  /**
   * Test that we can lock a context object against modification.
   */
  function testLockStatusExplcit() {
    $butler = new \Drupal\Context\Context();

    try {
      $butler->lock();
      // This should throw an exception.
      $butler->setValue('foo:bar', 'baz');

      $this->fail(t('Exception not thrown when setting context on a locked object.'));
    }
    catch (\Drupal\Context\LockedException $e) {
      $this->pass(t('Proper exception thrown when setting context on a locked object.'));
    }
    catch (Exception $e) {
      $this->fail(t('Incorrect exception thrown when modifying a locked object.'));
    }
  }

  /**
   * Test that we can lock a context object against modification.
   */
  function testLockStatusDerived() {
    $butler = new \Drupal\Context\Context();

    try {
      $butler->lock();
      // This should throw an exception.
      $butler->setHandler('foo:bar', 'ContextTestCaseHelperOne');

      $this->fail(t('Exception not thrown when setting context on a locked object.'));
    }
    catch (\Drupal\Context\LockedException $e) {
      $this->pass(t('Proper exception thrown when setting context handler on a locked object.'));
    }
    catch (Exception $e) {
      $this->fail(t('Incorrect exception thrown when modifying a locked object.'));
    }
  }

  /**
   * Test the result caching logic.
   */
  function testResultCaching() {
    $butler = new \Drupal\Context\Context();

    // This handler should return 1, then 2, then 3, each time it's called.
    $butler->setHandler('foo:bar', 'ContextTestCaseHelperTwo');
    $butler->lock();

    // Calling the same context variable should always give the same value
    // due to immutable caching.
    $this->assertEqual($butler->getValue('foo:bar'), $butler->getValue('foo:bar'), t('Identical context keys always return the same value.'));
    // Classes should be cached
    $this->assertNotEqual($butler->getValue('foo:bar'), $butler->getValue('foo:bar:baz'), t('Handler classes are cached.'));
  }

  function testTraversingLogic() {
    $butler = new \Drupal\Context\Context;
    // This handler will be more specific than the child one, but should not be
    // found because the less specialized child one will hide it.
    $butler->setHandler('foo:bar', 'ContextMockHandler', array('values' => array('baz' => 1)));

    $t1 = $butler->lock();
    $this->assertEqual(1, $butler->getValue('foo:bar:baz'), t('Asking for specialized value in root context.'));

    $b2 = $butler->addLayer();
    // This handler is less specialized than the parent one, but working on the
    // same context key path tree than its parent. It should hide the parent
    // more specialized one.
    $b2->setHandler('foo', 'ContextMockHandler', array('values' => array('bar:baz' => 2)));
    $t2 = $b2->lock();

    $this->assertEqual(2, $b2->getValue('foo:bar:baz'), t('Less specialized handler in the overriding context hides the root more specialized one.'));

    $b3 = $b2->addLayer();
    // Test the other way arround, local context handlers should always hide
    // the parent one if on the same context key path.
    $b3->setHandler('foo:bar', 'ContextMockHandler', array('values' => array('baz' => 3)));
    $t3 = $b3->lock();

    $this->assertEqual(3, $b3->getValue('foo:bar:baz'), t('More specialized handler in the overriding context hides the root less specialized one.'));

    $b4 = $b2->addLayer();

    // Set two handlers on the same context key tree, one less specialized
    // and one more specialized, and ensures that everything is ok.
    $b4->setHandler('foo', 'ContextMockHandler', array('values' => array('bar:baz' => 5)));
    $b4->setHandler('foo:bar', 'ContextMockHandler', array('values' => array('baz' => 7)));
    $t4 = $b4->lock();

    $this->assertEqual(7, $b4->getValue('foo:bar:baz'), t('Generic handler is overrided but the more specialized one in the fourth layer.'));

    $b5 = $b2->addLayer();

    // Set one handler and one value on the same context key tree, handler will
    // be more generic than the value.
    $b5->setHandler('foo', 'ContextMockHandler', array('values' => array('bar:baz' => 11)));
    $b5->setValue('foo:bar:baz', 13);
    $t5 = $b5->lock();

    $this->assertEqual(13, $b5->getValue('foo:bar:baz'), t('Value overrides the generic handler in fifth layer.'));
  }
}

class ContextValueObjectTestCase extends ContextTestCases {
  public static function getInfo() {
    return array(
      'name' => 'Context Value Object functionality',
      'description' => 'Test the Context system\'s handling of objects.',
      'group' => 'Context',
    );
  }

  /**
   * Test the retrieval of context values of objects.
   */
  function testContextValues() {
    $butler = new \Drupal\Context\Context();

    // Add a handler for this test to mimic to create an example (E.g. from url arguments)
    $butler->setHandler('example', 'ContextHandlerExample', array('id' => 1));

    $t1 = $butler->lock();

    // Catch the id from the url.
    $this->assertEqual($butler->getValue('example')->id, 1, t('ContextValueNode property id in context retrieved from request correctly.'));
    $this->assertEqual($butler->getValue('example')->foo, 10, t('ContextValueNode property foo in context retrieved from request correctly.'));

    // Add a context layer to the butler context.
    $b2 = $butler->addLayer();
    $b2->setValue('test', 'test');

    // Override with another instance of ContextValueInterface.
    $n1 = $this->createExampleObject(2);
    $b2->setValue('example', $n1);
    // This will cause the error, which needs to be fixed.
    //$b2['example:id'] = 2;
    $t2 = $b2->lock();

    $this->assertEqual($b2->getValue('test'), 'test', t('Explicit context assigned correctly in layer 2.'));
    $this->assertEqual($b2->getValue('example')->foo, 20, t('ContextValueNode property in in layer 2 overridden correctly.'));
    //$this->assertEqual($b2->getValue('example:id'), 2, t('Explicit context (example:id) overridden correctly in layer 2.'));

    // Add a context layer to the butler context.
    $b3 = $butler->addLayer();
    $b3->setValue('test', 'testischanged');
    $b3->setValue('test:foo', 'newtest');
    $n2 = $this->createExampleObject(3);
    $b3->setValue('example', $n2);
    // Setting the example:id as contextKey:offset will add that property to the context.
    $b3->setValue('example:id', 3);
    $t3 = $b3->lock();

    $this->assertEqual($b3->getValue('test'), 'testischanged', t('Explicit context overridden correctly in layer 3.'));
    $this->assertEqual($b3->getValue('test:foo'), 'newtest', t('Explicit context retrieved correctly in layer 3.'));
    $this->assertEqual($b3->getValue('example')->foo, 30, t('ContextValueNode property in in layer overridden correctly.'));
    $this->assertEqual($b3->getValue('example:id'), 3, t('Explicit context (example:id) overridden correctly in layer 3.'));
  }

  /**
   * Test that we can track the keys we've used.
   */
  public function testUsedKeys() {
    $butler = new \Drupal\Context\Context();

    // Add a handler for this test to mimic to create an example (E.g. from url arguments)
    $butler->setHandler('example', 'ContextHandlerExample', array('id' => 1));
    $butler->setValue('test', 'test');

    $t1 = $butler->lock();

    // Add a context layer to the butler context.
    $b2 = $butler->addLayer();
    $b2->setValue('test', 'test2');

    // Override with another instance of ContextValueInterface.
    $n1 = $this->createExampleObject(2);
    $b2->setValue('example', $n1);
    $t2 = $b2->lock();

    // Now try using the context values.
    $b2->getValue('example');
    $b2->getValue('test');

    $used = $b2->usedKeys();

    $this->assertEqual(count($used), 2, t('Correct number of keys in used keys array.'));
    $this->assertEqual($used['test'], 'test2', t('Used keys includes primitive key correctly.'));
    $this->assertEqual($used['example'], 2, t('Used keys includes object key correctly.'));
  }

  /**
   * Utility method for an example value object.
   *
   * The property values are predictable and can be used for testing.
   */
  protected function createExampleObject($id) {
    $c = new ContextValueExample($id);
    $c->foo = $id * 10;
    $c->bar = 'Baz ' . $id;

    return $c;
  }
}

/**
 * Test class for the mocking/overriding capability of the context system.
 */
class ContextMockTestCase extends ContextTestCases {
  public static function getInfo() {
    return array(
      'name' => 'Context Mocking functionality',
      'description' => 'Test the Context object\'s override capability.',
      'group' => 'Context',
    );
  }

  /**
   * Test basic context overrides.
   */
  function testOverrides() {
    $butler = new \Drupal\Context\Context();

    $butler->setHandler('http:get', 'ContextMockHandler', array('values' => array('foo' => 'bar')));

    $t1 = $butler->lock();

    $foo1 = $butler->getValue('http:get:foo');

    $this->assertEqual($foo1, 'bar', t('Correct Http GET fragment found.'));

    // Now check cloning.
    $b2 = $butler->addLayer();
    $b2->setValue('test', 'test');
    $t2 = $b2->lock();

    $this->assertNotIdentical($butler, $b2, t('New context object created properly.'));

    $this->assertEqual($b2->getValue('http:get:foo'), 'bar', t('Inherited property works.'));
    $this->assertEqual($b2->getValue('test'), 'test', t('Explicit context property in mocked object is correct.'));

    $b3 = $b2->addLayer();
    $b3->setValue('test', 'test_again');
    $t3 = $b3->lock();

    $this->assertEqual($b3->getValue('test'), 'test_again', t('Explicitly overriden property works.'));
  }

  /**
   * Test access to the "active" context.
   */
  public function testActiveContext() {
    $butler = new \Drupal\Context\Context();
    $t1 = $butler->lock();
    $b2 = $butler->addLayer();
    $t2 = $b2->lock();

    $this->assertEqual($b2, \Drupal\Context\Context::getActiveContext(), t('Active context is correct when adding context objects.'));

    unset($t2);

    $this->assertEqual($butler, \Drupal\Context\Context::getActiveContext(), t('Active context is correct when removing context objects.'));
  }

  /**
   * Test that cotext values are removed from the stack properly.
   */
  public function testGarbageCollection() {
    // Create a simple butler object.
    $butler = new \Drupal\Context\Context();
    $butler->setHandler('foo', 'ContextTestCaseHelperThree', array('bar' => 'butler'));
    $t1 = $butler->lock();

    // Create a new layer on top of that, which overrides some information.
    $b2 = $butler->addLayer();
    $b2->setHandler('foo', 'ContextTestCaseHelperThree', array('bar' => 'b2'));
    $t2 = $b2->lock();

    // Create a new layer on top of that, which actually does nothing at all.
    $b3 = $b2->addLayer();
    $t3 = $b3->lock();

    // Get rid of the tracker for b3.
    unset($t3);

    // Get rid of b2 entirely.
    unset($t2);
    unset($b2);

    try {
      // This should throw an exception.
      $b3->getValue('foo:bar');

      $this->fail(t('Exception not thrown when getting data from a context that should have been destroyed.'));
    }
    catch (\Drupal\Context\ParentContextNotExistsException $e) {
      $this->pass(t('Proper exception thrown when getting data from a context that should have been destroyed.'));
    }
    catch (Exception $e) {
      $this->fail(t('Incorrect exception thrown when getting data from a context that should have been destroyed.'));
    }
  }

  public function testContextArguments() {
    $butler = new \Drupal\Context\Context();
    $butler->setHandler('foo', 'ContextTestCaseHelperArguments');
    $butler->lock();

    $this->assertEqual($butler->getValue('foo:bar:baz'), array('bar', 'baz'), t('ContextHandler gets the correct arguments.'));
  }

  public function testInheritanceContexts() {
    $butler = new \Drupal\Context\Context();
    $butler->setHandler('foo:bar', 'ContextTestCaseHelperThree', array('baz' => 1));
    $t1 = $butler->lock();

    $b2 = $butler->addLayer();
    $b2->setHandler('foo', 'ContextTestCaseHelperThree', array('bar' => 3));
    $t2 = $b2->lock();

    $this->assertEqual($b2->getValue('foo:bar:baz'), 3, t('Context overrides even when more generic.'));

    unset($t2, $b2);

    $b3 = $butler->addLayer();
    $b3->setHandler('foo', 'ContextTestCaseHelperThree', array('baz' => 5));
    $t3 = $b3->lock();
    $this->assertEqual($b3->getValue('foo:bar:baz'), 1, t('If a context handler does not return data, the parent handler gets a chance to return data'));

    unset($t3, $b3);

    $b4 = $butler->addLayer();
    $b4->setHandler('foo:bar:baz', 'ContextTestCaseHelperThree', array('bax' => 7));
    $b4->setHandler('foo:bar', 'ContextTestCaseHelperThree', array('baz' => 9));
    $t4 = $b4->lock();

    $this->assertEqual($b4->getValue('foo:bar:baz:boo'), 9, t('A more generic handler in the same context should be called if the less generic handler does not return data.'));
  }

  /**
   * Test the offsetExists() method behavior.
   */
  public function testOffsetExists() {
    $butler = new \Drupal\Context\Context();
    $butler->setHandler('foo', 'ContextTestCaseHelperThree', array('bar' => 3));
    // B1: T1, B2: T2, Butler: Tutler :)
    $tutler = $butler->lock();

    // Normal behavior, with no derivation.
    $this->assertNotNull($butler->getValue('foo:bar'), t("foo:bar exists"));
    $this->assertNull($butler->getValue('I:Do:Not:Exist'), t("I:Do:Not:Exist does not exists"));
    $this->assertNotNull($butler->getValue('foo:bar'), t("foo:bar exists (caching does not break anything)"));
    $this->assertNull($butler->getValue('I:Do:Not:Exist'), t("I:Do:Not:Exist does not exists (caching does not break anything)"));
  }

  /**
   * Test the offsetExists() method behavior after multiple context derivation
   * (through multiple layers over the same root context).
   */
  public function testOffsetExistsWithDerivation() {
    $butler = new \Drupal\Context\Context();
    $butler->setHandler('foo', 'ContextTestCaseHelperThree', array('bar' => 3));
    // B1: T1, B2: T2, Butler: Tutler :)
    $tutler = $butler->lock();

    // Derivate and test over the new layer. Results should be the exact same.
    $b1 = $butler->addLayer();
    $t1 = $b1->lock();
    $this->assertNotNull($b1->getValue('foo:bar'), t("foo:bar exists in new layer"));
    $this->assertNull($b1->getValue('I:Do:Not:Exist'), t("I:Do:Not:Exist does not exists in new layer"));
    $this->assertNotNull($b1->getValue('foo:bar'), t("Value exists in new layer (caching does not break anything)"));
    $this->assertNull($b1->getValue('I:Do:Not:Exist'), t("I:Do:Not:Exist does not exists in new layer (caching does not break anything)"));

    unset($t1, $b1);

    // Derivate again, add a new value, and test for all other existence.
    $b2 = $butler->addLayer();
    $b2->setHandler('May:I', 'ContextTestCaseHelperThree', array('Exist' => 5));
    $t2 = $b2->lock();
    $this->assertNotNull($b2->getValue('foo:bar'), t("foo:bar exists in new layer"));
    $this->assertNull($b2->getValue('I:Do:Not:Exist'), t("I:Do:Not:Exist does not exists in new layer"));
    $this->assertNotNull($b2->getValue('May:I:Exist'), t("May:I:Exist value exists in new layer"));

    unset($t2, $tutler);
  }
}
